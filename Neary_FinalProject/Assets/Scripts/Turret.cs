﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private float InstantiationTimer = 2f;
    public GameObject TurretShot;
    public PlayerController Ship;
    public GameEnding ScoreManager;
    public GameObject Explosion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-3*Time.deltaTime, 0, 0);
        FireShot();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Projectile"))
        {
            
            ScoreManager.TurretScore();
            Instantiate(Explosion);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("OffScreen"))
        {
            Destroy(gameObject);
        }
    }
    void FireShot()
    {
        InstantiationTimer -= Time.deltaTime;
        if (InstantiationTimer <= 0)
        {
            Instantiate(TurretShot, gameObject.transform.position, transform.rotation * Quaternion.Euler(0, 0, 45));
            InstantiationTimer = 2f;
        }
    }
}
