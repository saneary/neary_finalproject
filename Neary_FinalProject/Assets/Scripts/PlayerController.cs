﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    public float speed = 10f;
    public GameObject Missile;
    public GameObject Bomb;
    public GameObject Explosion;
    
    public GameEnding SetGameOver;
    bool PlayerDead;
    // Start is called before the first frame update
    void Start()
    {
        PlayerDead = false;
        
        playerRb = GetComponent<Rigidbody>();
        
        

    }
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("EnemyProjectile"))
        {
            print("Game Over");
            PlayerDead = true;
            SetGameOver.SetPlayerDead();
            SetGameOver.SetScore();
            Destroy(other.gameObject);
            transform.GetChild(0).gameObject.SetActive(false);
            Instantiate(Explosion);

        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            print("Game Over");
            PlayerDead = true;
            SetGameOver.SetPlayerDead();
            SetGameOver.SetScore();
            Destroy(other.gameObject);
            transform.GetChild(0).gameObject.SetActive(false);
            Instantiate(Explosion);
        }
    }
    // Update is called once per frame
    
    
    
   
    void Update()
    {

        Vector3 pos = transform.position;
        if (PlayerDead == false)
        {
            if (Input.GetKey("w"))
            {
                pos.y += speed * Time.deltaTime;
            }
            if (Input.GetKey("s"))
            {
                pos.y -= speed * Time.deltaTime;
            }
            if (Input.GetKey("d"))
            {
                pos.x += speed * Time.deltaTime;
            }
            if (Input.GetKey("a"))
            {
                pos.x -= speed * Time.deltaTime;
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Instantiate(Bomb, gameObject.transform.position, transform.rotation * Quaternion.Euler(180, 0, 0));
                
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                Instantiate(Missile, gameObject.transform.position, transform.rotation * Quaternion.Euler(0, 0, 270));
            }
            transform.position = pos;

        }
        if (PlayerDead == true)
        {
            if (Input.GetKey("r"))
            {
                SceneManager.LoadScene(0);
            }
        }

    }
}



