﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameEnding : MonoBehaviour
{
    public Text ScoreText;
    public Text TurretText;
    public Text FighterText;
    public Text GameOver;
    public Text Restart;
    public static int PlayerScore;
    public static int TurretNumber;
    public static int FighterNumber;

    // Start is called before the first frame update
    void Start()
    {
        PlayerScore = 0;
        FighterNumber = 0;
        TurretNumber = 0;
        
    }
    
    public void FighterScore()
    {
        PlayerScore = PlayerScore + 50;
        FighterNumber = FighterNumber + 1;
        //SetScore();

        
    }
    public void TurretScore()
    {
        
        PlayerScore = PlayerScore + 100;
        TurretNumber = TurretNumber + 1;
        //SetScore();
        
    }
    
    public void SetScore()
    {
        ScoreText.text = "Total Score: " + PlayerScore;
        TurretText.text = "Turrets Destroyed: " + TurretNumber;
        FighterText.text = "Fighters Destroyed: " + FighterNumber;
        print(PlayerScore);
    }
    
    public void SetScoreTest()
    {
        ScoreText.text = "Fuck: " + PlayerScore;
        print(PlayerScore);
    }
    public void SetPlayerDead()
    {
        GameOver.text = "GAME OVER";
        Restart.text = "PRESS R TO RESTART";
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
