﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSpawner : MonoBehaviour
{
    public GameObject Turret;               
    private float InstantiationTimer = 5f;            
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void Spawn()
    {
        InstantiationTimer -= Time.deltaTime;
        if (InstantiationTimer <= 0)
        {
            int spawnPointX = Random.Range(25, 75);
            
            Vector3 spawnPosition = new Vector3(spawnPointX, -7, 0);
            Instantiate(Turret, spawnPosition, Quaternion.identity);
            InstantiationTimer = 5f;
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        Spawn();
    }
}
