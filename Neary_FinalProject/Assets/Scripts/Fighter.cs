﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fighter : MonoBehaviour
{
    private float InstantiationTimer = 2f;
    public GameObject FighterShot;
    public PlayerController Ship;
    public GameEnding ScoreManager;
    public GameObject Explosion;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0,0,5 * Time.deltaTime);
        FireShot();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Projectile"))
        {
            ScoreManager.FighterScore();
            Instantiate(Explosion);
            Destroy(other.gameObject);
            Destroy(gameObject);
        
        }
        if (other.gameObject.CompareTag("OffScreen"))
        {
            Destroy(gameObject);
            
        }
    }
    void FireShot()
    {
        InstantiationTimer -= Time.deltaTime;
        if (InstantiationTimer <= 0)
        {
            Instantiate(FighterShot, gameObject.transform.position, transform.rotation * Quaternion.Euler(90, 0, 0));
            InstantiationTimer = 2f;
        }
    }
}
