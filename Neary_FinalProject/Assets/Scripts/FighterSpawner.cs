﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterSpawner : MonoBehaviour
{
    public GameObject Turret;
    private float InstantiationTimer = 5f;

    // Start is called before the first frame update
    void Start()
    {

    }
    void Spawn()
    {
        InstantiationTimer -= Time.deltaTime;
        if (InstantiationTimer <= 0)
        {
            int spawnPointX = Random.Range(35, 85);
            int spawnPointY = Random.Range(-2, 8);
            
            Vector3 spawnPosition = new Vector3(spawnPointX, spawnPointY, 0);
            Instantiate(Turret, spawnPosition, transform.rotation * Quaternion.Euler(0, 270, 0));
            InstantiationTimer = 5f;
        }

    }
    // Update is called once per frame
    void Update()
    {
        Spawn();
    }
}
